module gitlab.bcowtech.de/bcow-go/forwarder-nsq

go 1.15

require github.com/nsqio/go-nsq v1.0.8
